/* global Firebase */
/* global CameraPopoverOptions */
/* global Camera */



angular.module('starter.controllers', [])



.controller('JourneyCtrl', function ($scope, DublinBikeService) {
    
    
    
    
    
})

.controller('MapCtrl',function ($scope, DublinBikeService)
{
    
    $scope.$on('mapInitialized', function (event, map){
        
       $scope.map = map;
       
   })
   
    $scope.dbs = DublinBikeService;
    
    $scope.dbs.load();
    //$scope.lat = 53.456;
   // $scope.lon = -6.456;
   //Import to init for map events
   
   $scope.showStationDetail = function (event, bikeStation) {
       
       $scope.dbs.currentBikeStation = bikeStation;
       
       $scope.map.showInfoWindow.apply(this,[event,'BikeStationInfo']);
       
       
       console.log("The name of the station is"  + bikeStation.name);
       
   }
    
})

.controller('StationCtrl',function ($scope, DublinBikeService)
{
    
    $scope.$on('mapInitialized', function (event, map){
        
       $scope.map = map;
       
   })
   


    
})

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'The Service', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
})



.controller('DashCtrl', function($scope, $firebaseArray, $cordovaCamera) {
  
  
   $scope.images = [];

    var ref = new Firebase("https://camerademo2.firebaseio.com/photos");
    var syncArray = $firebaseArray(ref);
    $scope.images = syncArray;

    $scope.upload = function() {
  
        var options = {
            quality : 75,
            destinationType : Camera.DestinationType.DATA_URL,
            sourceType : Camera.PictureSourceType.CAMERA,
            allowEdit : true,
            encodingType: Camera.EncodingType.JPEG,
            popoverOptions: CameraPopoverOptions,
            targetWidth: 500,
            targetHeight: 500,
            saveToPhotoAlbum: false
        };
        $cordovaCamera.getPicture(options).then(function(imageData) {
            syncArray.$add({image: imageData}).then(function() {
                alert("Image has been uploaded");
            });
        }, function(error) {
            console.error(error);
             alert(error);
        });
    }
    
  
  
  
  
  
  
  
  
});

