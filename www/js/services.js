
var app = angular.module('dublinbike.services',[]);

app.service("DublinBikeService", function($q, $http) {
    
    var self = {
        
        'lat' : 53.353462,
        'lon' : -6.265305,
        'results' : [],
        'currentBikeStation' : {},
        
        'load' : function () {
            var deferred = $q.defer();
            ionic.Platform.ready(function() {
                
                var bikeServiceUrl = "https://api.jcdecaux.com/vls/v1/stations?contract=dublin&apiKey=";
                var key = "3ee04e570025484107ab4ddbf9e613f59f8c8c4b";
               $http.get(bikeServiceUrl + key, {}).success().error();
                
                 $http.get(bikeServiceUrl + key, {}).success(function (data) {
                     
                     
                     console.log("We got data", data);
                     
                     angular.forEach(data, function(bikeStation)  {
                         
                         self.results.push(bikeStation);
                         
                         
                     })
                     
                     
                     
                     deferred.resolve();
                     
                 }).error(function () {
                     
                     
                     console.log("There was ann error");
                     
                 });
                
                
                
                
            }); // ionic platform ready
            
            
            
        }
        
        
        
    }
    
    
    return self;
    
    
    
});